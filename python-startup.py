import sys

def snipe_import_exceptions(exctype, value, traceback):
    if exctype == ImportError:
        install_module()
    else:
        sys.__excepthook__(exctype, value, traceback)

sys.excepthook = snipe_import_exceptions

def install_module():
    print "installing module"

